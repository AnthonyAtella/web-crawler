/**
 * "Fetches" a page in our mock internet.
 */
function fetchPage(pages, link) {
	console.log("Fetching " + link);
	var result = null;
	pages.forEach(function(page) {
		if(page.address === link) result = page;
	});
	return result;
}

/**
 * Crawls a page and all of its links. Reports
 * the results back to the parent thread via
 * message.
 */
function crawlPage(pages, page, results) {
	// TODO: Move fetch up to the main thread so multiple fetches don't happen on one page.
	var queue = [];
	page.links.forEach(function(link) {
		var linkPage = null;
		if(results.success.has(link)) {
			results.skipped.add(link);
		}
		else if(!results.error.has(link)
						&& (linkPage = fetchPage(pages, link)) !== null) {
			queue.push(linkPage);
			results.success.add(link);
		}
		else {
			results.error.add(link);
		}
	});
	postMessage({ queue: queue, results: results });
	close();
}

this.onmessage = function(event) {
	var pages = event.data.pages;
	var page = event.data.page;
	var results = event.data.results;
	console.log("Crawling " + page.address);
	crawlPage(pages, page, results);
};
