import { TestBed, inject } from '@angular/core/testing';
import { WebService } from './web.service';

describe('WebService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WebService]
    });
  });

  it('should be created', inject([WebService], (service: WebService) => {
    expect(service).toBeTruthy();
  }));

  it('should return real value', inject([WebService], (service: WebService) => {
    expect(service.getData(0)).not.toEqual(null);
  }));

  it('should return real value', inject([WebService], (service: WebService) => {
    expect(service.getData(1)).not.toEqual(null);
  }));
});
