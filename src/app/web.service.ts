import { Injectable } from '@angular/core';
import { DATA0 } from './data0';
import { DATA1 } from './data1';

@Injectable({
  providedIn: 'root'
})
export class WebService {

  constructor() { }

  getData(index: number) {
    if(index == 0) {
      return DATA0;
    }
    return DATA1;
  }

}
