import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { WebcrawlerComponent } from './webcrawler.component';
import { WebService } from '../web.service';
import { MatCardModule } from '@angular/material';

describe('WebcrawlerComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WebService]
    });
  });

  let component: WebcrawlerComponent;
  let fixture: ComponentFixture<WebcrawlerComponent>;
  let expectedDFS: object = {
    "success": [
      "http://foo.bar.com/p1",
      "http://foo.bar.com/p2",
      "http://foo.bar.com/p4",
      "http://foo.bar.com/p5",
      "http://foo.bar.com/p6"
    ],
    "skipped": [
      "http://foo.bar.com/p2",
      "http://foo.bar.com/p1",
      "http://foo.bar.com/p4",
      "http://foo.bar.com/p5"
    ],
    "error": [
      "http://foo.bar.com/p7",
      "http://foo.bar.com/p3"
    ]
  };
  let expectedBFS: object = {
    "success": [
      "http://foo.bar.com/p1",
      "http://foo.bar.com/p2",
      "http://foo.bar.com/p4",
      "http://foo.bar.com/p5",
      "http://foo.bar.com/p6"
    ],
    "skipped": [
      "http://foo.bar.com/p2",
      "http://foo.bar.com/p4",
      "http://foo.bar.com/p1",
      "http://foo.bar.com/p5"
    ],
    "error": [
      "http://foo.bar.com/p3",
      "http://foo.bar.com/p7"
    ]
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebcrawlerComponent ],
      imports: [
        MatCardModule,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebcrawlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain DFS results after DFS crawl', inject([WebService], (service: WebService) => {
    component.crawlDFS(service.getData(0));
    component.normalizeResults();
    expect(component.results).toEqual(expectedDFS);
  }));

  it('should contain BFS results after BFS crawl', inject([WebService], (service: WebService) => {
    component.crawlBFS(service.getData(0));
    component.normalizeResults();
    expect(component.results).toEqual(expectedBFS);
  }));
});
