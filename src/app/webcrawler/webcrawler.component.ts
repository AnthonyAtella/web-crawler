import { Component, OnInit, Input } from '@angular/core';
import { WebService } from '../web.service';

@Component({
  selector: 'app-webcrawler',
  templateUrl: './webcrawler.component.html',
  styleUrls: ['./webcrawler.component.css']
})
export class WebcrawlerComponent implements OnInit {
  @Input() dataIndex: number;
  data: any;
  results: any;

  constructor(private webService: WebService) { }

  ngOnInit() {
    this.data = this.webService.getData(this.dataIndex);
    this.results = {
      success: new Set(),
      skipped: new Set(),
      error: new Set()
    };
    //this.crawlDFS(this.data[0]);
    this.crawlBFS(this.data[0]);
    this.normalizeResults();
    //this.crawlBFSWW(this.data[0], 1);
  }

  /**
   * Converts the Sets in the results object into
   * Arrays for display.
   */
  normalizeResults() {
    this.results.success = Array.from(this.results.success);
    this.results.skipped = Array.from(this.results.skipped);
    this.results.error = Array.from(this.results.error);
  }

  /**
   * "Fetches" a page in our mock internet.
   */
  fetchPage(link: string) {
    var result = null;
    this.data.forEach(function(page) {
      if(page.address === link) result = page;
    });
    return result;
  }

  /**
   * Crawls a page and all of its links using depth
   * first search.
   */
  crawlDFS(page: any) {
    this.results.success.add(page.address);
    var ctx = this;
    page.links.forEach(function(link) {
      var p = null;
      if(ctx.results.success.has(link)) {
        ctx.results.skipped.add(link);
      }
      else if(!ctx.results.error.has(link)
              && (p = ctx.fetchPage(link)) !== null) {
        ctx.crawlDFS(p);
      }
      else {
        ctx.results.error.add(link);
      }
    });
  }

  /**
   * Crawls a page and all of its links using breadth
   * first search.
   */
  crawlBFS(page: any) {
    var queue = [ page ];
    this.results.success.add(page.address);
    while(queue.length > 0) {
      var p = queue.shift();
      var ctx = this;
      p.links.forEach(function(link) {
        var linkPage = null;
        if(ctx.results.success.has(link)) {
          ctx.results.skipped.add(link);
        }
        else if(!ctx.results.error.has(link)
                && (linkPage = ctx.fetchPage(link)) !== null) {
          queue.push(linkPage);
          ctx.results.success.add(link);
        }
        else {
          ctx.results.error.add(link);
        }
      });
    }
  }

  /**
   * Crawls a page and all of its links using breadth
   * first search. Uses Web Workers for multithreading.
   * This method is incomplete. Multiple fetches and
   * crawls happen per page.
   */
  crawlBFSWW(page, timeInterval) {
    var ctx = this;
    var queue = [ page ];
    this.results.success.add(page.address);
    var webWorkers = 0;
    var timer = setInterval(function() {
      if(queue.length > 0) {
        var p = queue.shift();
        var w = new Worker("../../assets/worker.js");
        webWorkers++;
        w.onmessage = function(event) {
          event.data.results.success.forEach(function(item) {
            ctx.results.success.add(item);
          });
          event.data.results.skipped.forEach(function(item) {
            ctx.results.skipped.add(item);
          });
          event.data.results.error.forEach(function(item) {
            ctx.results.error.add(item);
          });
          queue.push.apply(queue, event.data.queue);
          webWorkers--;
        };
        console.log("Spawning new thread for " + p.address);
        w.postMessage({ pages: ctx.data, page: p, results: ctx.results });
      }
      else if(webWorkers < 1) {
        clearInterval(timer);
        ctx.normalizeResults();
      }
    }, timeInterval);
  }

}
